data=read.table('Homicides.dat',header = T)

fit=glm(count~race,family=poisson(link=log),data = data)
summary(fit)
logLik(fit)
1-pchisq(844.71,df=1306)

library(MASS)
summary(glm.nb(count~race,data=data)) 
logLik(glm.nb(count~race,data=data))
1-pchisq(412.60,df=1306)
var(data$count)
0.2950959
mean(data$count)
0.1444954

exp(1.7331+0.2385*1.96)
exp(1.7331-0.2385*1.96)
exp(1.73314+0.14657*1.96)
exp(1.73314-0.14657*1.96)
