# Crab satellite counts

Crabs=read.table("Crabs.dat",header=T)
attach(Crabs)
Crabs

fit.pois=glm(y~weight,family=poisson,data=Crabs)
summary(fit.pois)
(X2=sum(residuals(fit.pois,type="pearson")^2))
(phi.hat=X2/(173-2))

summary(glm(y~weight,family=quasi(link="log",variance="mu"),data=Crabs))

summary(glm(y~weight,family=quasi(link="log",variance="mu^2"),data=Crabs))

library(MASS)
summary(glm.nb(y~weight,data=Crabs))


# Teratology study

Rats=read.table("Rats.dat",header=T)
attach(Rats)
Rats

placebo=ifelse(group==1,1,0)
fit.ML=glm(s/n~placebo+h,weights=n,family=binomial,data=Rats)
summary(fit.ML)
logLik(fit.ML)
1-pchisq(170.57,df=55)
(X2=sum(residuals(fit.ML,type="pearson")^2))
(phi.hat=X2/(58-3))

summary(glm(s/n~placebo+h,weights=n,family=quasi(link="logit",variance="mu(1-mu)"),data=Rats))

library(VGAM)
fit.bb=vglm(cbind(s,n-s)~placebo+h,betabinomial(zero=2,irho=.2),data=Rats)
# there are 2 parameters: mu & rho
# zero=2 specifies 0 covariates for paramter rho
# irho=.2 is initial guess for rho in beta-binomial variance
summary(fit.bb)
logit(-1.16744,inverse=T) # use 'inverse logit' (in VGAM) to obtain estimate of rho

library(aod)
quasibin(cbind(s,n-s)~placebo+h,data=Rats) # fits QL with beta-bin variance


# Crab satellite counts

library(gee)
Crabs=read.table("Crabs.dat",header=T)
attach(Crabs)
Crabs

obs=c(1:173) # label observations for GEE method
summary(gee(y~weight,id=obs,family=poisson,scale.fix=TRUE))


